<?xml version="1.0" ?><!DOCTYPE TS><TS language="tr" version="2.1">
<context>
    <name>AboutDialog</name>
    <message>
        <location filename="../qml/AboutDialog.qml" line="5"/>
        <source>About</source>
        <translation>Hakkında</translation>
    </message>
    <message>
        <location filename="../qml/AboutDialog.qml" line="19"/>
        <source>&lt;p&gt;This service is paid for entirely by donations from users like you. &lt;a href=&quot;%1&quot;&gt;Please donate&lt;/a&gt;.&lt;/p&gt;</source>
        <extracomment>donation text of the about dialog %1 -&gt; donation URL</extracomment>
        <translation>&lt;p&gt;Bu hizmetin bedeli, sizin gibi kullanıcıların bağışlarıyla sağlanıyor. &lt;a href=&quot;%1&quot;&gt;Lütfen bağış yapın&lt;/a&gt;.&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../qml/AboutDialog.qml" line="26"/>
        <source>&lt;p&gt;%1 is an easy, fast, and secure VPN service from %2. %1 does not require a user account, keep logs, or track you in any way.&lt;/p&gt; %3 &lt;p&gt;By using this application, you agree to the &lt;a href=&quot;%4&quot;&gt;Terms of Service&lt;/a&gt;. This service is provided as-is, without any warranty, and is intended for people who work to make the world a better place.&lt;/p&gt;</source>
        <extracomment>about dialog %1 -&gt; application name %2 -&gt; provider name %3 -&gt; donation text if activated %4 -&gt; TOS URL</extracomment>
        <translation>&lt;p&gt;%1, %2 tarafından sunulan kolay, hızlı ve güvenli bir VPN hizmetidir. %1 kullanıcı hesabı gerektirmiyor, günlük tutmuyor veya sizi hiç bir şekilde izlemiyor.&lt;/p&gt; %3 &lt;p&gt;Bu uygulamayı kullanarak, &lt;a href=&quot;%4&quot;&gt;Hizmet Kullanım Şartlarını&lt;/a&gt; kabul etmiş olursunuz. Bu hizmet olduğu gibi, herhangi bir garanti olmadan sunulmaktadır ve dünyayı daha iyi bir yer yapmak için çalışan insanlara yöneliktir.&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../qml/AboutDialog.qml" line="36"/>
        <source>%1 version: %2</source>
        <extracomment>%1 -&gt; application name %2 -&gt; version string</extracomment>
        <translation>%1 sürüm: %2</translation>
    </message>
</context>
<context>
    <name>DonateDialog</name>
    <message>
        <location filename="../qml/DonateDialog.qml" line="6"/>
        <source>Donate</source>
        <translation>Bağış Yapın</translation>
    </message>
    <message>
        <location filename="../qml/DonateDialog.qml" line="14"/>
        <source>The %1 service is expensive to run. Because we don't want to store personal information about you, there are no accounts or billing for this service. But if you want the service to continue, donate at least $5 each month.

Do you want to donate now?</source>
        <extracomment>donate dialog %1 -&gt; application name</extracomment>
        <translation>%1 hizmetini çalıştırmak pahalıdır. Sizin hakkınızda kişisel bilgi saklamak istemediğimiz için, bu hizmete ilişkin hesaplar veya fatura yok. Ancak hizmetin sürmesini istiyorsanız, her ay en azından 5$ bağış yapmalısınız.

Şimdi bağış yapmak ister misiniz?</translation>
    </message>
</context>
<context>
    <name>FailDialog</name>
    <message>
        <location filename="../qml/FailDialog.qml" line="5"/>
        <source>Initialization Error</source>
        <translation>Başlatma Hatası</translation>
    </message>
</context>
<context>
    <name>LoginDialog</name>
    <message>
        <location filename="../qml/LoginDialog.qml" line="6"/>
        <source>Login</source>
        <translation>Giriş</translation>
    </message>
    <message>
        <location filename="../qml/LoginDialog.qml" line="20"/>
        <source>Patron ID</source>
        <translation>Patron Kimliği</translation>
    </message>
    <message>
        <location filename="../qml/LoginDialog.qml" line="24"/>
        <source>Password</source>
        <translation>Şifre</translation>
    </message>
    <message>
        <location filename="../qml/LoginDialog.qml" line="35"/>
        <source>Enter your Patron ID</source>
        <translation>Patron kimliğinizi girin</translation>
    </message>
    <message>
        <location filename="../qml/LoginDialog.qml" line="37"/>
        <source>Log in with your library credentials</source>
        <translation>Kütüphane kimliğinizle oturum açın</translation>
    </message>
    <message>
        <location filename="../qml/LoginDialog.qml" line="42"/>
        <source>You can check your Patron ID number in the back of your library card</source>
        <translation>Patron kimlik numaranız, kütüphane kartınızın arkadasındadır.</translation>
    </message>
</context>
<context>
    <name>LoginOKDialog</name>
    <message>
        <location filename="../qml/LoginOKDialog.qml" line="7"/>
        <source>Login Successful</source>
        <translation>Giriş Başarılı</translation>
    </message>
    <message>
        <location filename="../qml/LoginOKDialog.qml" line="11"/>
        <source>Login successful. You can now start the VPN.</source>
        <translation>Giriş başarılı. VPN&apos;i şimdi başlatabilirsiniz.</translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <location filename="../qml/main.qml" line="38"/>
        <source>Could not find helpers. Please check your installation</source>
        <translation>Yardımcılar bulunamıyor, lütfen kurulumunuzu kontrol edin.</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="40"/>
        <source>Could not find polkit agent.</source>
        <translation>polkit vekili bulunamıyor.</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="55"/>
        <source>Oops! The authentication service seems down. Please try again later</source>
        <translation>Eyvah! Kimlik doğrulama hizmeti çalışmıyor gözüküyor. Lütfen daha sonra tekrar deneyin</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="56"/>
        <source>Service Error</source>
        <translation>Hizmet Hatası</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="62"/>
        <source>Please check your Patron ID</source>
        <translation>Lütfen Patron kimliğinizi kontrol edin</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="64"/>
        <source>Could not log in with those credentials, please retry</source>
        <translation>Bu kimlik bilgileriyle giriş yapılamıyor, lütfen tekrar deneyin</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="66"/>
        <source>Login Error</source>
        <translation>Giriş Hatası</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="70"/>
        <source>Got an error starting %1: %2</source>
        <extracomment>%1 -&gt; application name %2 -&gt; error string</extracomment>
        <translation>%1 başlatılırken bir hata oluştu: %2</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="104"/>
        <source>%1 off</source>
        <extracomment>%1 -&gt; application name</extracomment>
        <translation>%1 kapalı</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="107"/>
        <source>%1 on</source>
        <extracomment>%1 -&gt; application name</extracomment>
        <translation>%1 açık</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="110"/>
        <source>Connecting to %1</source>
        <extracomment>%1 -&gt; application name</extracomment>
        <translation>%1 uygulamasına bağlanılıyor</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="113"/>
        <source>Stopping %1</source>
        <extracomment>%1 -&gt; application name</extracomment>
        <translation>%1 durduruluyor</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="116"/>
        <source>%1 blocking internet</source>
        <extracomment>%1 -&gt; application name</extracomment>
        <translation>%1 interneti engelliyor</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="163"/>
        <location filename="../qml/main.qml" line="211"/>
        <source>Checking status...</source>
        <translation>Durum denetimi...</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="218"/>
        <source>Reconnect</source>
        <translation>Yeniden bağlan</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="220"/>
        <source>Turn on</source>
        <translation>Aç</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="231"/>
        <source>Cancel</source>
        <translation>İptal</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="233"/>
        <source>Turn off</source>
        <translation>Kapat</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="244"/>
        <source>Help...</source>
        <translation>Yardım...</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="254"/>
        <source>Donate...</source>
        <translation>Bağış yap...</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="260"/>
        <source>About...</source>
        <translation>Hakkında...</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="267"/>
        <source>Quit</source>
        <translation>Çıkış</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="297"/>
        <source>Error starting VPN</source>
        <translation>VPN başlatma hatası</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="307"/>
        <source>Missing authentication agent</source>
        <translation>Kimlik doğrulama vekili bulunamıyor</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="308"/>
        <source>Could not find a polkit authentication agent. Please run one and try again.</source>
        <translation>Bir polkit kimlik doğrulama vekili bulunamıyor. Bir tane çalıştırın ve tekrar deneyin.</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="76"/>
        <source>: a fast and secure VPN. Powered by Bitmask.</source>
        <translation>: hızlı ve güvenli bir VPN. Bitmask tarafından sunuluyor.</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="83"/>
        <source>Do not show the systray icon (useful together with Gnome Shell extension, or to control VPN by other means).</source>
        <translation>Sistem çubuğu simgesini gösterme (Gnome Kabuk eklentisiyle birlikte veya VPN&apos;i başka şekillerde denetlemek için kullanışlıdır)</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="90"/>
        <source>Enable Web API.</source>
        <translation>Web API&apos;yi etkinleştir</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="96"/>
        <source>Install helpers (Linux only, requires sudo).</source>
        <translation>Yardımcıları kur (sadece Linux, sudo gerekiyor)</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="102"/>
        <source>Use obfs4 to obfuscate the traffic, if available in the provider.</source>
        <translation>Trafiği gizlemek için obfs4 kullan, sadece sağlayıcıda kullanılabiliyorsa</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="108"/>
        <source>Disable autostart for the next run.</source>
        <translation>Sonraki çalıştırma için otomatik başlatmayı devre dışı bırak</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="113"/>
        <source>Web API port (default: 8080)</source>
        <translation>Web API portu (varsayılan: 8080)</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="115"/>
        <source>Start the VPN, either &apos;on&apos; or &apos;off&apos;.</source>
        <translation>VPN&apos;i başlat, &apos;açık&apos; veya &apos;kapalı&apos;.</translation>
    </message>
</context>
</TS>