<?xml version="1.0" ?><!DOCTYPE TS><TS language="fr" version="2.1">
<context>
    <name>AboutDialog</name>
    <message>
        <location filename="../qml/AboutDialog.qml" line="5"/>
        <source>About</source>
        <translation>À propos</translation>
    </message>
    <message>
        <location filename="../qml/AboutDialog.qml" line="19"/>
        <source>&lt;p&gt;This service is paid for entirely by donations from users like you. &lt;a href=&quot;%1&quot;&gt;Please donate&lt;/a&gt;.&lt;/p&gt;</source>
        <extracomment>donation text of the about dialog %1 -&gt; donation URL</extracomment>
        <translation>&lt;p&gt;Ce service est entièrement financé par les dons d’utilisateurs tels que vous. &lt;a href=&quot;%1&quot;&gt;Veuillez faire un don&lt;/a&gt;.&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../qml/AboutDialog.qml" line="26"/>
        <source>&lt;p&gt;%1 is an easy, fast, and secure VPN service from %2. %1 does not require a user account, keep logs, or track you in any way.&lt;/p&gt; %3 &lt;p&gt;By using this application, you agree to the &lt;a href=&quot;%4&quot;&gt;Terms of Service&lt;/a&gt;. This service is provided as-is, without any warranty, and is intended for people who work to make the world a better place.&lt;/p&gt;</source>
        <extracomment>about dialog %1 -&gt; application name %2 -&gt; provider name %3 -&gt; donation text if activated %4 -&gt; TOS URL</extracomment>
        <translation>&lt;p&gt;%1 est un service de RPV simple, rapide et sécurisé offert par %2. %1 n’exige aucun compte utilisateur, ne conserve aucun journal, ni ne vous suit à la trace d’aucune manière.&lt;/p&gt; %3 &lt;p&gt;En utilisant cette application, vous acceptez les &lt;a href=&quot;%4&quot;&gt;Conditions générales d’utilisation&lt;/a&gt;. Ce service est fourni tel quel, sans aucune garantie et s’adresse aux personnes qui œuvrent à la création d’un monde meilleur.&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../qml/AboutDialog.qml" line="36"/>
        <source>%1 version: %2</source>
        <extracomment>%1 -&gt; application name %2 -&gt; version string</extracomment>
        <translation>%1 version : %2</translation>
    </message>
</context>
<context>
    <name>DonateDialog</name>
    <message>
        <location filename="../qml/DonateDialog.qml" line="6"/>
        <source>Donate</source>
        <translation>Faire un don</translation>
    </message>
    <message>
        <location filename="../qml/DonateDialog.qml" line="14"/>
        <source>The %1 service is expensive to run. Because we don't want to store personal information about you, there are no accounts or billing for this service. But if you want the service to continue, donate at least $5 each month.

Do you want to donate now?</source>
        <extracomment>donate dialog %1 -&gt; application name</extracomment>
        <translation>L’exploitation du service %1 coûte cher. Dans la mesure où ne nous voulons enregistrer aucun renseignement personnel à votre sujet, il n’y a ni compte ni facturation pour ce service. Mais si vous souhaitez toutefois que le service perdure, faites un don d’au moins 5 $ mensuellement.

Voulez-vous faire un don maintenant ?</translation>
    </message>
</context>
<context>
    <name>FailDialog</name>
    <message>
        <location filename="../qml/FailDialog.qml" line="5"/>
        <source>Initialization Error</source>
        <translation>Erreur d’initialisation</translation>
    </message>
</context>
<context>
    <name>LoginDialog</name>
    <message>
        <location filename="../qml/LoginDialog.qml" line="6"/>
        <source>Login</source>
        <translation>Connexion</translation>
    </message>
    <message>
        <location filename="../qml/LoginDialog.qml" line="20"/>
        <source>Patron ID</source>
        <translation>ID Patron</translation>
    </message>
    <message>
        <location filename="../qml/LoginDialog.qml" line="24"/>
        <source>Password</source>
        <translation>Mot de passe</translation>
    </message>
    <message>
        <location filename="../qml/LoginDialog.qml" line="35"/>
        <source>Enter your Patron ID</source>
        <translation>Saisissez votre ID Patron</translation>
    </message>
    <message>
        <location filename="../qml/LoginDialog.qml" line="37"/>
        <source>Log in with your library credentials</source>
        <translation>Connectez-vous avec vos identifiants pour la papeterie</translation>
    </message>
    <message>
        <location filename="../qml/LoginDialog.qml" line="42"/>
        <source>You can check your Patron ID number in the back of your library card</source>
        <translation>Vous pouvez vérifier votre numéro d’ID Patron au verso de votre carte de papeterie</translation>
    </message>
</context>
<context>
    <name>LoginOKDialog</name>
    <message>
        <location filename="../qml/LoginOKDialog.qml" line="7"/>
        <source>Login Successful</source>
        <translation>La connexion est réussie</translation>
    </message>
    <message>
        <location filename="../qml/LoginOKDialog.qml" line="11"/>
        <source>Login successful. You can now start the VPN.</source>
        <translation>La connexion est réussie. Vous pouvez maintenant lancer le RPV.</translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <location filename="../qml/main.qml" line="38"/>
        <source>Could not find helpers. Please check your installation</source>
        <translation>Impossible de trouver les aides. Veuillez vérifier votre installation</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="40"/>
        <source>Could not find polkit agent.</source>
        <translation>Impossible de trouver l’agent polkit</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="55"/>
        <source>Oops! The authentication service seems down. Please try again later</source>
        <translation>Oups. Le service d&apos;authentification semble être hors service. Veuillez essayer plus tard</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="56"/>
        <source>Service Error</source>
        <translation>Erreur de service</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="62"/>
        <source>Please check your Patron ID</source>
        <translation>Veuillez confirmer votre ID Patron</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="64"/>
        <source>Could not log in with those credentials, please retry</source>
        <translation>Impossible de se connecter avec ces identifiants, veuillez réessayer</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="66"/>
        <source>Login Error</source>
        <translation>Erreur de connexion</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="70"/>
        <source>Got an error starting %1: %2</source>
        <extracomment>%1 -&gt; application name %2 -&gt; error string</extracomment>
        <translation>Erreur de démarrage de %1 : %2</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="104"/>
        <source>%1 off</source>
        <extracomment>%1 -&gt; application name</extracomment>
        <translation>%1 désactivé</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="107"/>
        <source>%1 on</source>
        <extracomment>%1 -&gt; application name</extracomment>
        <translation>%1 activé</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="110"/>
        <source>Connecting to %1</source>
        <extracomment>%1 -&gt; application name</extracomment>
        <translation>Connexion à %1</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="113"/>
        <source>Stopping %1</source>
        <extracomment>%1 -&gt; application name</extracomment>
        <translation>Arrêt de %1</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="116"/>
        <source>%1 blocking internet</source>
        <extracomment>%1 -&gt; application name</extracomment>
        <translation>%1 bloque Internet</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="163"/>
        <location filename="../qml/main.qml" line="211"/>
        <source>Checking status...</source>
        <translation>Vérification de l’état…</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="218"/>
        <source>Reconnect</source>
        <translation>Se reconnecter</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="220"/>
        <source>Turn on</source>
        <translation>Activer</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="231"/>
        <source>Cancel</source>
        <translation>Annuler</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="233"/>
        <source>Turn off</source>
        <translation>Désactiver</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="244"/>
        <source>Help...</source>
        <translation>Aide…</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="254"/>
        <source>Donate...</source>
        <translation>Faire un don…</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="260"/>
        <source>About...</source>
        <translation>À propos…</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="267"/>
        <source>Quit</source>
        <translation>Fermer</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="297"/>
        <source>Error starting VPN</source>
        <translation>Erreur de démarrage du RPV</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="307"/>
        <source>Missing authentication agent</source>
        <translation>L’agent d’authentification manque</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="308"/>
        <source>Could not find a polkit authentication agent. Please run one and try again.</source>
        <translation>Impossible de trouver un agent d’authentification polkit. Veuillez en exécuter un et réessayer.</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="76"/>
        <source>: a fast and secure VPN. Powered by Bitmask.</source>
        <translation> : un RPV rapide et sécurisé. Propulsé par Bitmask.</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="83"/>
        <source>Do not show the systray icon (useful together with Gnome Shell extension, or to control VPN by other means).</source>
        <translation>Ne pas afficher l’icône de la zone de notification (utile combiné à l’extension Gnome Shell ou pour contrôler le RPV par d’autres moyens).</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="90"/>
        <source>Enable Web API.</source>
        <translation>Activer l’API Web.</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="96"/>
        <source>Install helpers (Linux only, requires sudo).</source>
        <translation>Installer les aides (Linux seulement, « sudo » est requis). </translation>
    </message>
    <message>
        <location filename="../main.cpp" line="102"/>
        <source>Use obfs4 to obfuscate the traffic, if available in the provider.</source>
        <translation>Utiliser obfs4 pour brouiller le trafic, si proposé par le fournisseur.</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="108"/>
        <source>Disable autostart for the next run.</source>
        <translation>Désactiver le démarrage automatique pour la prochaine exécution.</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="113"/>
        <source>Web API port (default: 8080)</source>
        <translation>Port de l’API Web (par défaut : 8080)</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="115"/>
        <source>Start the VPN, either &apos;on&apos; or &apos;off&apos;.</source>
        <translation>Démarrer le RPV, soit Activer soit Désactiver</translation>
    </message>
</context>
</TS>