<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1">
<context>
    <name>AboutDialog</name>
    <message>
        <location filename="../qml/AboutDialog.qml" line="5"/>
        <source>About</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/AboutDialog.qml" line="18"/>
        <source>&lt;p&gt;This service is paid for entirely by donations from users like you. &lt;a href=&quot;%1&quot;&gt;Please donate&lt;/a&gt;.&lt;/p&gt;</source>
        <extracomment>donation text of the about dialog</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/AboutDialog.qml" line="26"/>
        <source>&lt;p&gt;%1 is an easy, fast, and secure VPN service from %2. %1 does not require a user account, keep logs, or track you in any way.&lt;/p&gt; %3 &lt;p&gt;By using this application, you agree to the &lt;a href=&quot;%4&quot;&gt;Terms of Service&lt;/a&gt;. This service is provided as-is, without any warranty, and is intended for people who work to make the world a better place.&lt;/p&gt;</source>
        <extracomment>about dialog %1 -&gt; application name %2 -&gt; provider name %3 -&gt; donation text if activated</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/AboutDialog.qml" line="37"/>
        <source>%1 version: %2</source>
        <extracomment>%1 -&gt; application name %2 -&gt; version string</extracomment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DonateDialog</name>
    <message>
        <location filename="../qml/DonateDialog.qml" line="6"/>
        <source>Donate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/DonateDialog.qml" line="14"/>
        <source>The %1 service is expensive to run. Because we don&apos;t want to store personal information about you, there are no accounts or billing for this service. But if you want the service to continue, donate at least $5 each month.

Do you want to donate now?</source>
        <extracomment>donate dialog %1 -&gt; application name</extracomment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>FailDialog</name>
    <message>
        <location filename="../qml/FailDialog.qml" line="5"/>
        <source>Initialization Error</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LoginDialog</name>
    <message>
        <location filename="../qml/LoginDialog.qml" line="6"/>
        <source>Login</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/LoginDialog.qml" line="21"/>
        <source>Patron ID</source>
        <extracomment>Ask for the library card number</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/LoginDialog.qml" line="25"/>
        <source>Password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/LoginDialog.qml" line="37"/>
        <source>Enter your Patron ID</source>
        <extracomment>Ask for the library card number</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/LoginDialog.qml" line="39"/>
        <source>Log in with your library credentials</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/LoginDialog.qml" line="44"/>
        <source>You can check your Patron ID number in the back of your library card</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LoginOKDialog</name>
    <message>
        <location filename="../qml/LoginOKDialog.qml" line="7"/>
        <source>Login Successful</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/LoginOKDialog.qml" line="11"/>
        <source>Login successful. You can now start the VPN.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <location filename="../qml/main.qml" line="39"/>
        <source>Could not find helpers. Please check your installation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="41"/>
        <source>Could not find polkit agent.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="57"/>
        <source>Oops! The authentication service seems down. Please try again later</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="58"/>
        <source>Service Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="64"/>
        <source>Please check your Patron ID</source>
        <extracomment>Incorrect library card number</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="66"/>
        <source>Could not log in with those credentials, please retry</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="68"/>
        <source>Login Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="72"/>
        <source>Got an error starting %1: %2</source>
        <extracomment>%1 -&gt; application name %2 -&gt; error string</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="107"/>
        <source>%1 off</source>
        <extracomment>%1 -&gt; application name</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="110"/>
        <source>%1 on</source>
        <extracomment>%1 -&gt; application name</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="113"/>
        <source>Connecting to %1</source>
        <extracomment>%1 -&gt; application name</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="116"/>
        <source>Stopping %1</source>
        <extracomment>%1 -&gt; application name</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="119"/>
        <source>%1 blocking internet</source>
        <extracomment>%1 -&gt; application name</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="171"/>
        <location filename="../qml/main.qml" line="273"/>
        <source>Checking status...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="280"/>
        <source>Reconnect</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="282"/>
        <source>Turn on</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="294"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="296"/>
        <source>Turn off</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="326"/>
        <source>Help...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="316"/>
        <source>Donate...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="308"/>
        <source>About...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="335"/>
        <source>Report a bug...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="347"/>
        <source>Quit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="377"/>
        <source>Error starting VPN</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="387"/>
        <source>Missing authentication agent</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="388"/>
        <source>Could not find a polkit authentication agent. Please run one and try again.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../main.cpp" line="76"/>
        <source>: a fast and secure VPN. Powered by Bitmask.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../main.cpp" line="83"/>
        <source>Do not show the systray icon (useful together with Gnome Shell extension, or to control VPN by other means).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../main.cpp" line="90"/>
        <source>Enable Web API.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../main.cpp" line="96"/>
        <source>Install helpers (Linux only, requires sudo).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../main.cpp" line="102"/>
        <source>Use obfs4 to obfuscate the traffic, if available in the provider.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../main.cpp" line="108"/>
        <source>Disable autostart for the next run.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../main.cpp" line="113"/>
        <source>Web API port (default: 8080)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../main.cpp" line="115"/>
        <source>Start the VPN, either &apos;on&apos; or &apos;off&apos;.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
