<?xml version="1.0" ?><!DOCTYPE TS><TS language="lt" version="2.1">
<context>
    <name>AboutDialog</name>
    <message>
        <location filename="../qml/AboutDialog.qml" line="5"/>
        <source>About</source>
        <translation>Apie</translation>
    </message>
    <message>
        <location filename="../qml/AboutDialog.qml" line="19"/>
        <source>&lt;p&gt;This service is paid for entirely by donations from users like you. &lt;a href=&quot;%1&quot;&gt;Please donate&lt;/a&gt;.&lt;/p&gt;</source>
        <extracomment>donation text of the about dialog %1 -&gt; donation URL</extracomment>
        <translation>&lt;p&gt;Ši paslauga yra pilnai apmokama iš tokių pačių naudotojų, kaip jūs, paaukojimų. &lt;a href=&quot;%1&quot;&gt;Prašome paaukoti&lt;/a&gt;.&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../qml/AboutDialog.qml" line="26"/>
        <source>&lt;p&gt;%1 is an easy, fast, and secure VPN service from %2. %1 does not require a user account, keep logs, or track you in any way.&lt;/p&gt; %3 &lt;p&gt;By using this application, you agree to the &lt;a href=&quot;%4&quot;&gt;Terms of Service&lt;/a&gt;. This service is provided as-is, without any warranty, and is intended for people who work to make the world a better place.&lt;/p&gt;</source>
        <extracomment>about dialog %1 -&gt; application name %2 -&gt; provider name %3 -&gt; donation text if activated %4 -&gt; TOS URL</extracomment>
        <translation>&lt;p&gt;%1 yra lengva naudoti, greita ir saugi VPN paslauga iš %2. %1 nereikalauja naudotojo paskyros, nesaugo žurnalų ir jokiu kitu būdu jūsų neseka.&lt;/p&gt; %3 &lt;p&gt;Naudodami šią programą, sutinkate su &lt;a href=&quot;%4&quot;&gt;Naudojimosi sąlygomis&lt;/a&gt;. Ši paslauga yra teikiama esamu pavidalu, be jokių garantijų ir yra skirta žmonėms, kurie dirba, kad padarytų pasaulį geresnį.&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../qml/AboutDialog.qml" line="36"/>
        <source>%1 version: %2</source>
        <extracomment>%1 -&gt; application name %2 -&gt; version string</extracomment>
        <translation>%1 versija: %2</translation>
    </message>
</context>
<context>
    <name>DonateDialog</name>
    <message>
        <location filename="../qml/DonateDialog.qml" line="6"/>
        <source>Donate</source>
        <translation>Paaukoti</translation>
    </message>
    <message>
        <location filename="../qml/DonateDialog.qml" line="14"/>
        <source>The %1 service is expensive to run. Because we don't want to store personal information about you, there are no accounts or billing for this service. But if you want the service to continue, donate at least $5 each month.

Do you want to donate now?</source>
        <extracomment>donate dialog %1 -&gt; application name</extracomment>
        <translation>%1 paslaugą brangiai kainuoja išlaikyti. Kadangi nenorime laikyti jūsų asmeninės informacijos, todėl nėra jokių paskyrų ar apmokestinimo už šią paslaugą. Tačiau, jei norite, kad paslauga būtų teikiama ir toliau, paaukokite kiekvieną mėnesį bent po $5 (JAV dolerius).

Ar norėtumėte paaukoti dabar?</translation>
    </message>
</context>
<context>
    <name>FailDialog</name>
    <message>
        <location filename="../qml/FailDialog.qml" line="5"/>
        <source>Initialization Error</source>
        <translation>Inicijavimo klaida</translation>
    </message>
</context>
<context>
    <name>LoginDialog</name>
    <message>
        <location filename="../qml/LoginDialog.qml" line="6"/>
        <source>Login</source>
        <translation>Prisijungti</translation>
    </message>
    <message>
        <location filename="../qml/LoginDialog.qml" line="20"/>
        <source>Patron ID</source>
        <translation>Patron ID</translation>
    </message>
    <message>
        <location filename="../qml/LoginDialog.qml" line="24"/>
        <source>Password</source>
        <translation>Slaptažodis</translation>
    </message>
    <message>
        <location filename="../qml/LoginDialog.qml" line="35"/>
        <source>Enter your Patron ID</source>
        <translation>Įveskite savo Patron ID</translation>
    </message>
    <message>
        <location filename="../qml/LoginDialog.qml" line="37"/>
        <source>Log in with your library credentials</source>
        <translation>Prisijungti naudojant savo bibliotekos prisijungimo duomenis</translation>
    </message>
    <message>
        <location filename="../qml/LoginDialog.qml" line="42"/>
        <source>You can check your Patron ID number in the back of your library card</source>
        <translation>Savo Patron ID galite rasti kitoje bibliotekos kortelės pusėje</translation>
    </message>
</context>
<context>
    <name>LoginOKDialog</name>
    <message>
        <location filename="../qml/LoginOKDialog.qml" line="7"/>
        <source>Login Successful</source>
        <translation>Prisijungimas sėkmingas</translation>
    </message>
    <message>
        <location filename="../qml/LoginOKDialog.qml" line="11"/>
        <source>Login successful. You can now start the VPN.</source>
        <translation>Prisijungimas sėkmingas. Dabar, galite paleisti VPN.</translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <location filename="../qml/main.qml" line="38"/>
        <source>Could not find helpers. Please check your installation</source>
        <translation>Nepavyko rasti pagelbiklių. Patikrinkite savo diegimą</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="40"/>
        <source>Could not find polkit agent.</source>
        <translation>Nepavyko rasti polkit agento.</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="55"/>
        <source>Oops! The authentication service seems down. Please try again later</source>
        <translation>Oi! Atrodo, kad tapatybės nustatymo paslauga neveikia. Vėliau bandykite dar kartą</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="56"/>
        <source>Service Error</source>
        <translation>Paslaugos klaida</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="62"/>
        <source>Please check your Patron ID</source>
        <translation>Pasitikrinkite Patron ID</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="64"/>
        <source>Could not log in with those credentials, please retry</source>
        <translation>Nepavyko prisijungti naudojant tuos prisijungimo duomenis, bandykite dar kartą</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="66"/>
        <source>Login Error</source>
        <translation>Prisijungimo klaida</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="70"/>
        <source>Got an error starting %1: %2</source>
        <extracomment>%1 -&gt; application name %2 -&gt; error string</extracomment>
        <translation>Paleidžiant %1, gauta klaida: %2</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="104"/>
        <source>%1 off</source>
        <extracomment>%1 -&gt; application name</extracomment>
        <translation>%1 išjungta</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="107"/>
        <source>%1 on</source>
        <extracomment>%1 -&gt; application name</extracomment>
        <translation>%1 įjungta</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="110"/>
        <source>Connecting to %1</source>
        <extracomment>%1 -&gt; application name</extracomment>
        <translation>Jungiamasi prie %1</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="113"/>
        <source>Stopping %1</source>
        <extracomment>%1 -&gt; application name</extracomment>
        <translation>Stabdoma %1</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="116"/>
        <source>%1 blocking internet</source>
        <extracomment>%1 -&gt; application name</extracomment>
        <translation>%1 blokuoja internetą</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="163"/>
        <location filename="../qml/main.qml" line="211"/>
        <source>Checking status...</source>
        <translation>Tikrinama būsena...</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="218"/>
        <source>Reconnect</source>
        <translation>Prisijungti iš naujo</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="220"/>
        <source>Turn on</source>
        <translation>Įjungti</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="231"/>
        <source>Cancel</source>
        <translation>Atšaukti</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="233"/>
        <source>Turn off</source>
        <translation>Išjungti</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="244"/>
        <source>Help...</source>
        <translation>Pagalba...</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="254"/>
        <source>Donate...</source>
        <translation>Paaukoti...</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="260"/>
        <source>About...</source>
        <translation>Apie...</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="267"/>
        <source>Quit</source>
        <translation>Išeiti</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="297"/>
        <source>Error starting VPN</source>
        <translation>Klaida paleidžiant VPN</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="307"/>
        <source>Missing authentication agent</source>
        <translation>Trūksta tapatybės nustatymo agento</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="308"/>
        <source>Could not find a polkit authentication agent. Please run one and try again.</source>
        <translation>Nepavyko rasti politikos rinkinio tapatybės nustatymo agento. Paleiskite jį ir bandykite dar kartą.</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="76"/>
        <source>: a fast and secure VPN. Powered by Bitmask.</source>
        <translation>: greitas ir saugus VPN. Veikia su Bitmask.</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="83"/>
        <source>Do not show the systray icon (useful together with Gnome Shell extension, or to control VPN by other means).</source>
        <translation>Nerodyti sistemos dėklo piktogramos (praverčia su Gnome apvalkalo plėtiniu arba siekiant kitokiu būdu valdyti VPN).</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="90"/>
        <source>Enable Web API.</source>
        <translation>Įjungti saityno API.</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="96"/>
        <source>Install helpers (Linux only, requires sudo).</source>
        <translation>Įdiegti pagelbiklius (tik Linux, reikalauja sudo).</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="102"/>
        <source>Use obfs4 to obfuscate the traffic, if available in the provider.</source>
        <translation>Naudoti obfs4, siekiant maskuoti duomenų srautą, jei prieinama iš teikėjo.</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="108"/>
        <source>Disable autostart for the next run.</source>
        <translation>Išjungti automatinį paleidimą kitam paleidimui.</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="113"/>
        <source>Web API port (default: 8080)</source>
        <translation>Saityno API prievadas (numatytasis: 8080)</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="115"/>
        <source>Start the VPN, either &apos;on&apos; or &apos;off&apos;.</source>
        <translation>Paleisti VPN, arba „on“, arba „off“.</translation>
    </message>
</context>
</TS>