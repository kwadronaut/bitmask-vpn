<?xml version="1.0" ?><!DOCTYPE TS><TS language="et" version="2.1">
<context>
    <name>AboutDialog</name>
    <message>
        <location filename="../qml/AboutDialog.qml" line="5"/>
        <source>About</source>
        <translation>Info</translation>
    </message>
    <message>
        <location filename="../qml/AboutDialog.qml" line="19"/>
        <source>&lt;p&gt;This service is paid for entirely by donations from users like you. &lt;a href=&quot;%1&quot;&gt;Please donate&lt;/a&gt;.&lt;/p&gt;</source>
        <extracomment>donation text of the about dialog %1 -&gt; donation URL</extracomment>
        <translation>&lt;p&gt;Selle teenuse eest makstakse täielikult sinusuguste kasutajate poolt tulnud vabatahtlike annetustega &lt;a href=&quot;%1&quot;&gt;Palun anneta&lt;/a&gt;.&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../qml/AboutDialog.qml" line="26"/>
        <source>&lt;p&gt;%1 is an easy, fast, and secure VPN service from %2. %1 does not require a user account, keep logs, or track you in any way.&lt;/p&gt; %3 &lt;p&gt;By using this application, you agree to the &lt;a href=&quot;%4&quot;&gt;Terms of Service&lt;/a&gt;. This service is provided as-is, without any warranty, and is intended for people who work to make the world a better place.&lt;/p&gt;</source>
        <extracomment>about dialog %1 -&gt; application name %2 -&gt; provider name %3 -&gt; donation text if activated %4 -&gt; TOS URL</extracomment>
        <translation>&lt;p&gt;%1 on lihtne, kiire ja turvaline VPN teenus %2. %1 ei nõua kasutajakonto olemas olu, ei säilita logisid ega jälgi sind mingil viisil.&lt;/p&gt; %3 &lt;p&gt;Selle rakenduse kasutamisel nõustud &lt;a href=&quot;%4&quot;&gt;teenuse tingimustega&lt;/a&gt;. Seda teenust pakutakse ilma mingisuguste garantiideta ning on mõeldud inimestele, kes töötavad selle nimel, et teha maailma paremaks.&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../qml/AboutDialog.qml" line="36"/>
        <source>%1 version: %2</source>
        <extracomment>%1 -&gt; application name %2 -&gt; version string</extracomment>
        <translation>%1 versioon: %2</translation>
    </message>
</context>
<context>
    <name>DonateDialog</name>
    <message>
        <location filename="../qml/DonateDialog.qml" line="6"/>
        <source>Donate</source>
        <translation>Anneta</translation>
    </message>
    <message>
        <location filename="../qml/DonateDialog.qml" line="14"/>
        <source>The %1 service is expensive to run. Because we don't want to store personal information about you, there are no accounts or billing for this service. But if you want the service to continue, donate at least $5 each month.

Do you want to donate now?</source>
        <extracomment>donate dialog %1 -&gt; application name</extracomment>
        <translation>Teenuse %1 töös hoidmine on kallis. Kuna me ei soovi sinu kohta isiklike andmeid säilitada, siis pole selle teenuse jaoks mingeid kasutajakontosid või arveid. Aga kui soovid, et teenus saaks jätkuda, siis palun anneta vähemalt 5 eurot kuus.

Kas soovid kohe annetada?</translation>
    </message>
</context>
<context>
    <name>FailDialog</name>
    <message>
        <location filename="../qml/FailDialog.qml" line="5"/>
        <source>Initialization Error</source>
        <translation>Käivitamise tõrge</translation>
    </message>
</context>
<context>
    <name>LoginDialog</name>
    <message>
        <location filename="../qml/LoginDialog.qml" line="6"/>
        <source>Login</source>
        <translation>Kasutajanimi</translation>
    </message>
    <message>
        <location filename="../qml/LoginDialog.qml" line="20"/>
        <source>Patron ID</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../qml/LoginDialog.qml" line="24"/>
        <source>Password</source>
        <translation>Parool</translation>
    </message>
    <message>
        <location filename="../qml/LoginDialog.qml" line="35"/>
        <source>Enter your Patron ID</source>
        <translation>Sisesta oma Partron ID</translation>
    </message>
    <message>
        <location filename="../qml/LoginDialog.qml" line="37"/>
        <source>Log in with your library credentials</source>
        <translation>Sinu reegi kasutajaandmetega sisselogimine</translation>
    </message>
    <message>
        <location filename="../qml/LoginDialog.qml" line="42"/>
        <source>You can check your Patron ID number in the back of your library card</source>
        <translation>Oma Patroni ID numbrit saad kontrollida oma raamatukogukaardi tagant</translation>
    </message>
</context>
<context>
    <name>LoginOKDialog</name>
    <message>
        <location filename="../qml/LoginOKDialog.qml" line="7"/>
        <source>Login Successful</source>
        <translation>Sisselogimine oli edukas</translation>
    </message>
    <message>
        <location filename="../qml/LoginOKDialog.qml" line="11"/>
        <source>Login successful. You can now start the VPN.</source>
        <translation>Sisselogimine oli edukas. Võid nüüd VPN-i käivitada.</translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <location filename="../qml/main.qml" line="38"/>
        <source>Could not find helpers. Please check your installation</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../qml/main.qml" line="40"/>
        <source>Could not find polkit agent.</source>
        <translation>Polkit agenti ei leitud.</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="55"/>
        <source>Oops! The authentication service seems down. Please try again later</source>
        <translation>Oih! Tundub, et autentimise teenus on maas. Palun proovi hiljem uuesti</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="56"/>
        <source>Service Error</source>
        <translation>Teenuse tõrge</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="62"/>
        <source>Please check your Patron ID</source>
        <translation>Palun kontrolli oma Patron ID-d</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="64"/>
        <source>Could not log in with those credentials, please retry</source>
        <translation>Nende andmetega ei õnnestunud sisse logida. Palun proovi uuesti</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="66"/>
        <source>Login Error</source>
        <translation>Sisselogimise tõrge</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="70"/>
        <source>Got an error starting %1: %2</source>
        <extracomment>%1 -&gt; application name %2 -&gt; error string</extracomment>
        <translation>Rakendust %1 käivitades anti veateade: %2</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="104"/>
        <source>%1 off</source>
        <extracomment>%1 -&gt; application name</extracomment>
        <translation>%1 väljas</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="107"/>
        <source>%1 on</source>
        <extracomment>%1 -&gt; application name</extracomment>
        <translation>%1 sees</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="110"/>
        <source>Connecting to %1</source>
        <extracomment>%1 -&gt; application name</extracomment>
        <translation>Ühendumine rakendusega %1</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="113"/>
        <source>Stopping %1</source>
        <extracomment>%1 -&gt; application name</extracomment>
        <translation>%1 peatamine</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="116"/>
        <source>%1 blocking internet</source>
        <extracomment>%1 -&gt; application name</extracomment>
        <translation>%1 - internet blokeeritud</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="163"/>
        <location filename="../qml/main.qml" line="211"/>
        <source>Checking status...</source>
        <translation>Staatuse kontrollimine...</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="218"/>
        <source>Reconnect</source>
        <translation>Ühenda uuesti</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="220"/>
        <source>Turn on</source>
        <translation>Lülita sisse</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="231"/>
        <source>Cancel</source>
        <translation>Loobu</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="233"/>
        <source>Turn off</source>
        <translation>Lülita välja</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="244"/>
        <source>Help...</source>
        <translation>Abiinfo...</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="254"/>
        <source>Donate...</source>
        <translation>Anneta...</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="260"/>
        <source>About...</source>
        <translation>Info...</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="267"/>
        <source>Quit</source>
        <translation>Välju</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="297"/>
        <source>Error starting VPN</source>
        <translation>Tõrge VPN-i käivitamisel</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="307"/>
        <source>Missing authentication agent</source>
        <translation>Autentimise agent puudub</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="308"/>
        <source>Could not find a polkit authentication agent. Please run one and try again.</source>
        <translation>Polkit autentimise agenti ei leitud. Palun käivita see ning proovi siis uuesti.</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="76"/>
        <source>: a fast and secure VPN. Powered by Bitmask.</source>
        <translation>: kiire ja turvaline VPN. Kasutatud tarkvara on Bitmask.</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="83"/>
        <source>Do not show the systray icon (useful together with Gnome Shell extension, or to control VPN by other means).</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../main.cpp" line="90"/>
        <source>Enable Web API.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../main.cpp" line="96"/>
        <source>Install helpers (Linux only, requires sudo).</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../main.cpp" line="102"/>
        <source>Use obfs4 to obfuscate the traffic, if available in the provider.</source>
        <translation>Kui see on teenusepakkuja poolt saadaval, siis kasuta liikluse peitmiseks teeki obfs4.</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="108"/>
        <source>Disable autostart for the next run.</source>
        <translation>Keela järgmisel käivitamisel automaatne käivitamine.</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="113"/>
        <source>Web API port (default: 8080)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../main.cpp" line="115"/>
        <source>Start the VPN, either &apos;on&apos; or &apos;off&apos;.</source>
        <translation type="unfinished"/>
    </message>
</context>
</TS>